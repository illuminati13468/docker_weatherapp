### Weatherapp

This is a simple weatherapp built in node.js. It displays the current weather, temperature and if it is sunny/clouded 
and so on.
I use api.openweather.org to supply with the weather information on a city location. 

#### Requirements:
You need docker and docker compose installed
You need node.js
